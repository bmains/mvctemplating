﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MVCTemplating;


namespace System.Web.Mvc
{
	public static class HttpContextExtensions
	{

		public static TemplateConfiguration GetConfiguration(this HttpContext context, string name)
		{
			return TemplateConfigurations.Configurations.FirstOrDefault(i => i.Name == name);
		}

		public static TemplateConfiguration GetConfiguration(this HttpContextBase context, string name)
		{
			return TemplateConfigurations.Configurations.FirstOrDefault(i => i.Name == name);
		}

	}
}
