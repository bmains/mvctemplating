﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCTemplating;


namespace System.Web.Mvc
{
	public static class ViewContextExtensions
	{

		public static TemplateConfiguration GetConfiguration(this ViewContext context, string name)
		{
			return TemplateConfigurations.Configurations.FirstOrDefault(i => i.Name == name);
		}

	}
}
