﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTemplating.Demo.Controllers
{
	[TemplateAttribute]
    public class SamplesController : Controller
    {

		public ActionResult Forms()
		{
			return View();
		}

		public ActionResult Functions()
		{
			return View();
		}
        
        public ActionResult Helpers()
        {
            return View();
        }

    }
}
