﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Routing;

using MVCTemplating;
using MVCTemplating.Views;
using MVCTemplating.Views.Discovery;
using MVCTemplating.Views.Generation;


namespace MVCTemplating
{
    public abstract class BaseRenderController : Controller
    {
		[NonAction]
		private void CopyContent(View view, string directory)
		{
			var path = Server.MapPath("~/Views/" + view.Controller.GetType().Name.Replace("Controller", ""));
			var files = Directory.GetFiles(path).Where(i => i.EndsWith(".css") || i.EndsWith(".js"));

			foreach (var file in files)
			{
				System.IO.File.Copy(file, Path.Combine(directory, Path.GetFileName(file)), true);
			}
		}

		[NonAction]
		private ViewCollection FindViews()
		{
			var path = Server.MapPath("~");
			var discovery = new ViewDiscoveryStrategy();

			return discovery.FindViews(new DiscoveryOptions { ProjectFolder = path });
		}

		[NonAction]
		private string GenerateHtmlForView(View view, TemplateConfiguration template)
		{
			var gen = new ViewGenerator();
			return gen.Generate(new ViewGenerationOptions 
			{ 
				Controller = view.Controller, 
				Method = view.ViewName,
				RootUrl = template.Rendering.RootUrl
			});
		}

        protected ActionResult RenderViews(string configurationName)
		{
			if (string.IsNullOrEmpty(configurationName))
				throw new InvalidOperationException("A template name is required.");

			var configuration = TemplateConfigurations.Configurations.First(i => i.Name == configurationName);
			var views = this.FindViews();
			string output = this.HttpContext.Server.MapPath(configuration.Rendering.OutputDirectory);
			
			foreach (var view in views)
			{
				string folder = output + @"\" + view.Controller.GetType().Name.Replace("Controller", "");

				if (!Directory.Exists(folder))
					Directory.CreateDirectory(folder);

				CopyContent(view, folder);

				string fileName = folder + @"\" + view.ViewName + configuration.Rendering.FileExtension;
				//string viewContent = this.RenderRazorViewToString(view.Controller, view.ViewName, null);
				string viewContent = GenerateHtmlForView(view, configuration);

				System.IO.File.WriteAllText(fileName, viewContent);
			}

			return Content("OK");
        }

    }
}
