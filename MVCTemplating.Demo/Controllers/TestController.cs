﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using MVCTemplating.Views.Discovery;
using MVCTemplating.Views.Generation;


namespace MVCTemplating.Demo.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/

		public ActionResult FindControllers()
		{
			var path = Server.MapPath("~");
			var discovery = new ViewDiscoveryStrategy();

			var views = discovery.FindViews(new DiscoveryOptions { ProjectFolder = path });

			ViewBag.Views = views;
			return View();
		}

        public ActionResult GenerateHtml()
        {
			var baseUrl = "http://localhost/PhoneGapMvcDemo/";

			var gen = new ViewGenerator();
			return Content(gen.Generate(new ViewGenerationOptions { Controller = new SamplesController(), Method = "Functions" }));
			//return Content(RenderRazorViewToString(new HomeController(), "Index", null));
        }

		//private string RenderRazorViewToString(ControllerBase controller, string viewName, object model)
		//{
		//	ViewData.Model = model;

		//	var routeData = new RouteData();
		//	routeData.Values.Add("controller", "Home");
		//	routeData.Values.Add("action", "Index");


		//	var ctx = new ControllerContext(new RequestContext(this.HttpContext, routeData), controller);

		//	using (var sw = new StringWriter())
		//	{
		//		var viewResult = ViewEngines.Engines.FindView(ctx, viewName, null);
		//		var viewContext = new ViewContext(ctx, viewResult.View, ViewData, TempData, sw);

		//		viewResult.View.Render(viewContext, sw);
		//		viewResult.ViewEngine.ReleaseView(ctx, viewResult.View);
		//		return sw.GetStringBuilder().ToString();
		//	}
		//}

    }
}
