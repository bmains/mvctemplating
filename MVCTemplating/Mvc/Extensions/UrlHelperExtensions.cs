﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web.Routing;


namespace System.Web.Mvc
{
	public static class UrlHelperExtensions
	{

		public static string GenerateAction(this UrlHelper url)
		{

			return url.Action();
		}



		public static string GenerateAction(this UrlHelper url, string action)
		{
			return url.Action(action);
		}

		public static string GenerateAction(this UrlHelper url, string action, object routeValues)
		{
			return url.Action(action, routeValues);
		}

		public static string GenerateAction(this UrlHelper url, string action, RouteValueDictionary routeValues)
		{
			return url.Action(action, routeValues);
		}



		public static string GenerateAction(this UrlHelper url, string action, string controller)
		{
			return url.Action(action, controller);
		}

		public static string GenerateAction(this UrlHelper url, string action, string controller, object routeValues)
		{
			return url.Action(action, controller, routeValues);
		}

		public static string GenerateAction(this UrlHelper url, string action, string controller, RouteValueDictionary routeValues)
		{
			return url.Action(action, controller, routeValues);
		}

	}
}
