﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTemplating;


namespace MVCTemplating.Demo.Controllers
{
	
	public class HomeController : Controller
	{
		[TemplateAttribute]
		public ActionResult Index()
		{
			ViewBag.Message = "Welcome to ASP.NET MVC!";

			return View();
		}
	}
}
