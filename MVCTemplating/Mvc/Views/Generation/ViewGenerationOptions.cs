﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace MVCTemplating.Views.Generation
{
	public class ViewGenerationOptions
	{
		private string _rootUrl = "http://localhost";



		public ControllerBase Controller { get; set; }

		public string ControllerName
		{
			get { return Controller.GetType().Name; }
		}

		public string FileName
		{
			get { return Method + ".cshtml"; }
		}

		public string Method { get; set; }

		public string RootUrl 
		{
			get { return _rootUrl; }
			set { _rootUrl = value; }
		}

	}
}
