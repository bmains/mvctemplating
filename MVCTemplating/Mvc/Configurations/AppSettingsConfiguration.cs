﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace MVCTemplating
{
	public class AppSettingsConfiguration
	{
		private IDictionary<string, object> _values = null;
		


		public IDictionary<string, object> Values
		{
			get 
			{
				if (_values == null)
					_values = new Dictionary<string, object>();

				return _values; 
			}
		}



		public AppSettingsConfiguration AddValue(string key, object value)
		{
			this.Values.Add(key, value);
			return this;
		}

	}
}
