﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web.Mvc;


namespace MVCTemplating.Views.Discovery
{
	public class ViewDiscoveryStrategy : IViewDiscoveryStrategy
	{
		public ViewCollection FindViews(DiscoveryOptions options)
		{
			if (options == null)
				throw new ArgumentNullException("options");

			string binPath = Path.Combine(options.ProjectFolder, "bin");
			if (!Directory.Exists(binPath))
				throw new DirectoryNotFoundException(string.Format("Directory '{0}' not found.", binPath));

			var controllerTypes = new List<Type>();

			Directory.EnumerateFiles(binPath, "*.dll").ToList().ForEach((path) =>
				{
					Assembly assembly = Assembly.LoadFile(path);
					controllerTypes.AddRange(assembly.GetTypes().Where(i => typeof(ControllerBase).IsAssignableFrom(i)));
				});

			var outputList = new ViewCollection();

			foreach (var controllerType in controllerTypes)
			{
				var methods = controllerType.GetMethods().Where(i => i.ReturnType != null && typeof(ActionResult).IsAssignableFrom(i.ReturnType));
				var controller = (ControllerBase)Activator.CreateInstance(controllerType);

				if (controllerType.GetCustomAttributes(typeof(TemplateAttribute), false).FirstOrDefault() != null)
				{
					outputList.AddRange(methods.Select(i => new View { Controller = controller, ViewName = i.Name }));
				}
				else
				{
					outputList.AddRange(
						methods
							.Where(i => i.GetCustomAttributes(typeof(TemplateAttribute), false).FirstOrDefault() != null)
							.Select(i => new View { Controller = controller, ViewName = i.Name })
					);
				}

			}

			return outputList;
		}
	}
}
