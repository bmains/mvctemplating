using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace MVCTemplating.Views.Generation
{
	public interface IViewGenerator
	{
		string Generate(ViewGenerationOptions options);
	}
}