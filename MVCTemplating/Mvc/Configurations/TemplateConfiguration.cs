﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCTemplating
{
	public class TemplateConfiguration
	{
		private AppSettingsConfiguration _appSettings = new AppSettingsConfiguration();
		private RenderConfiguration _rendering = new RenderConfiguration();
		private string _name = null;



		public AppSettingsConfiguration AppSettings
		{
			get { return _appSettings; }
		}

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public RenderConfiguration Rendering
		{
			get { return _rendering; }
		}



		public TemplateConfiguration AppSettingsConfiguration(Action<AppSettingsConfiguration> fn)
		{
			if (_appSettings == null)
				_appSettings = new AppSettingsConfiguration();

			fn(_appSettings);

			return this;
		}

		public TemplateConfiguration RenderingConfiguration(Action<RenderConfiguration> fn)
		{
			if (_rendering == null)
				_rendering = new RenderConfiguration();

			fn(_rendering);

			return this;
		}



	}
}
