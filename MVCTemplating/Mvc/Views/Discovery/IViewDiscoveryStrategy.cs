﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCTemplating.Views.Discovery
{
	public interface IViewDiscoveryStrategy
	{
		ViewCollection FindViews(DiscoveryOptions options);
	}
}
