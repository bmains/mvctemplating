﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Web.Routing;


namespace MVCTemplating.Views.Generation
{
	public class ViewGenerator : IViewGenerator
	{
		public string Generate(ViewGenerationOptions options)
		{
			if (options == null)
				throw new ArgumentNullException("options");

			var routeData = new RouteData();
			routeData.Values.Add("controller", options.Controller.GetType().Name.Replace("Controller", ""));
			routeData.Values.Add("action", options.Method);

			var rootUrl = options.RootUrl ?? "http://localhost/";
			if (!rootUrl.EndsWith("/"))
				rootUrl += "/";

			var request = new HttpRequest(options.FileName,
				String.Concat(rootUrl, options.ControllerName, "/", options.Method), "");

			var outputWriter = new StringWriter();
			var viewData = new ViewDataDictionary();
			var tempData = new TempDataDictionary();
			var httpContext = new HttpContext(request, new HttpResponse(outputWriter));
			
			var controllerContext = new ControllerContext(
				new RequestContext(new HttpContextWrapper(httpContext), routeData), 
				options.Controller);

			options.Controller.ControllerContext = controllerContext;
			options.Controller.TempData = tempData;
			options.Controller.ViewData = viewData;

			using (var sw = new StringWriter())
			{
				var viewResult = ViewEngines.Engines.FindView(controllerContext, options.Method, null);
				var viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, sw);

				viewResult.View.Render(viewContext, sw);
				viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
				return sw.GetStringBuilder().ToString();
			}
		}
	}
}
