﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace MVCTemplating.Views
{
	public class View
	{
		public ControllerBase Controller 
		{ 
			get; 
			set; 
		}

		public string ViewName
		{
			get;
			set;
		}
	}
}
