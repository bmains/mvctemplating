﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace MVCTemplating
{
	public class RenderConfiguration
	{
		private string _fileExtension = ".html";
		private string _outputDirectory = "~/Build/www";
		private string _rootUrl = "http://localhost/";


		
		public string FileExtension
		{
			get { return _fileExtension; }
			set { _fileExtension = value; }
		}

		public string OutputDirectory
		{
			get { return _outputDirectory; }
			set { _outputDirectory = value; }
		}

		public string RootUrl
		{
			get { return _rootUrl; }
			set { _rootUrl = value; }
		}

	}
}
